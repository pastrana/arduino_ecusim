#include "mcp.h"
#include "libCAN.h"
#include "libSPI.h"
#include "SoftwareSerial.h"
#include <string.h>
#include <SPI.h>


void setup() {
  Serial.begin(9600);
  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));
  /* Config interruptions */
  attachInterrupt(0, check_INT, FALLING);
  Can.reset_MCP();
  Can.CAN_Init();
}



void check_INT() {
  byte tipe = 1;
  unsigned char intFlags = Can.CAN_Read(MCP_CANINTF);
  messageCAN structMessage;

  do {
    if (intFlags & 0b10000000) {
      Serial.print("\r\nKind of Interrupt");
      switch (tipe) {
        case 1: Serial.print("\r\nInterrupt pending in MERRF - Message Error Interrupt Flag");
          break;
        case 2: Serial.print("\r\nInterrupt pending in WAKIF - WakeUp");
          break;
        case 3: Serial.print("\r\nInterrupt pending in ERRIF - Multiple sources in EFLG register");
          break;
        case 4: Serial.print("\r\nInterrupt pending in TX2IF - Empty");
          break;
        case 5: Serial.print("\r\nInterrupt pending in TX1IF - Empty");
          break;
        case 6: Serial.print("\r\nInterrupt pending in TX0IF - Empty");
          break;
        case 7: Serial.print("\r\nInterrupt pending in RX1IF\n");
          structMessage = Can.CAN_Read_RXB1();
          break;
        case 8: Serial.print("\r\nInterrupt pending in RX0IF");
          structMessage = Can.CAN_Read_RXB0();
          break;
        default: Serial.print("\r\nOther interrupt or error has ocurred");
          tipe = 0;
      }
    }
    tipe++;
    intFlags = intFlags << 1;
  } while (Can.CAN_Read(intFlags) == 0x00);
  //All flags down
  Can.CAN_Write(MCP_CANINTF, 0x00);

  if (structMessage.ID == 0x7DF) {
    Serial.print("\r\nOBD Query received");
    check_OBD_query(structMessage);
  }

}

void check_OBD_query(messageCAN structMessage)
{

}

void sendAnswer(messageCAN canMessage)
{
  messageCAN structMessage;
  structMessage.lenght = canMessage.lenght;
  structMessage.RTR = 0;
  structMessage.ID = canMessage.ID;
  int j;
  while (j < structMessage.lenght) {
    structMessage.data[j] = random(48, 58);
    Serial.println(structMessage.data[j]);
    j++;
  }
  Can.CAN_Tx_TXB0(structMessage);
}
void loop() {
  delay(100);
}
