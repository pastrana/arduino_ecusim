#include "libCAN.h"
#include "libSPI.h"
#include "mcp.h"

#include <SPI.h>


#define  MCP_WRITE    0x02
#define  MCP_READ     0x03
#define  MCP_RESET    0xC0

/******************
* Private methods *
******************/

/* Configuration of the CAN speed, It has been
 * configured to send data to 125kbps.
 */
int configCANSpeed() {

  //125.000
  Can.CAN_Write(MCP_CNF1, 0x07);
  Can.CAN_Write(MCP_CNF2, 0x98);
  Can.CAN_Write(MCP_CNF3, 0x03);

  //500.000

  /*Can.CAN_Write(MCP_CNF1, 0x01);
    Can.CAN_Write(MCP_CNF2, 0x98);
    Can.CAN_Write(MCP_CNF3, 0x03);*/
    
}

int configCANInterrupt() {
  /* Interrupt */
  Can.CAN_Write(MCP_CANINTE, 0xE3); // 11100011         Interruption                         Enabled /Disabled
                                    //        . RXB0=1	RXB0 BUFFER 0                        ENABLED
                                    //       .  RXB1=1	RXB1 BUFFER 1                        ENABLED
                                    //      .   TXB0=0	TXB0 BECOMING EMPTY                  DISABLED
                                    //     .    TXB1=0	TXB1 BECOMING EMPTY                  DISABLED
                                    //    .     TXB2=0	TXB2 BECOMING EMPTY                  DISABLED
                                    //   .      ERRIE=1	EFLG ERROR CONDITION CHANGE          ENABLED
                                    //  .       WAKIE=1	WAKEUP INTERRUPT                     ENABLED
                                    //.         MERRE=1	ERROR DURING MESSAGE RX/TX           ENABLED
  Serial.print("\r\nCAN_INTE=");
  Serial.print(Can.CAN_Read(MCP_CANINTE));
  return 0;
}


/**
 * First function to call, reset all configurations
 * of MCP2515.
 */

int libCAN::reset_MCP() {
  ObjSPI.SPI_Sel();
  SPI.transfer(MCP_RESET);
  ObjSPI.SPI_Unsel();
  Serial.print("\r\nMCP reset OK!!");
  delay(10);
  return 0;
}

/* Write in a register of MCP2515
 * It only needs the address and the value that you
 * want to write in */
int libCAN::CAN_Write(unsigned char address, unsigned char value) {
  ObjSPI.SPI_Sel();
  SPI.transfer(MCP_WRITE);
  SPI.transfer(address);
  SPI.transfer(value);
  ObjSPI.SPI_Unsel();
  return 0;
}

/* Read in a register of MCP2515
 * It only needs the address and the value that you
 * want to write in */
unsigned char libCAN::CAN_Read(unsigned char address) {
  unsigned char c;
  ObjSPI.SPI_Sel();
  SPI.transfer(MCP_READ);
  SPI.transfer(address);
  c = SPI.transfer(0);
  ObjSPI.SPI_Unsel();
  return c;
}

/**
 * Second function to call, it allows to configure the
 * MCP2515 in normal operation mode.
 */

int libCAN::CAN_Init() {
  /* MCP2515 configuration mode (0x80)*/
  CAN_Write(MCP_CANCTRL, 0x80);
  while ((CAN_Read(MCP_CANSTAT) & 0xE0) != 0x80) {
    Serial.print("\r\nCAN_STAT=");
    Serial.print(CAN_Read(MCP_CANSTAT));
  }
  if (configCANSpeed() != 0) return 1;
  if (configCANInterrupt() != 0) return 1;
  /*MCP2515 normalconfiguration mode */
  CAN_Write(MCP_CANCTRL, 0x00);
  Serial.print("\r\nCAN_STAT2=");
  Serial.print(CAN_Read(MCP_CANSTAT));
  return 0;
}

/**
 * This method allows to send a CAN message through the first buffer
 * TXB0
 */
int  libCAN::CAN_Tx_TXB0(messageCAN message) {

  int i = 0;
  unsigned char cdata;
  unsigned long time;
  time = millis();
  //Waiting until TXREQ=0 -> No pending transmisions
  while (CAN_Read(MCP_TXB0CTRL) & 0x08) {
    Serial.print("\r\nCAN_TXB0CTRL=");
    Serial.print(CAN_Read(MCP_TXB0CTRL));
    if ((millis() - time) > 3000) return 1;
    delay(200);
  }
  //Clearing TXREQ and setting up the higher priority
  CAN_Write(MCP_TXB0CTRL, 0x03);
  //ID to registers (5b each one)
  //High bits of the standar ID
  CAN_Write(MCP_TXB0SIDH, (message.ID >> 3));
  //Low bits of the standar ID
  CAN_Write(MCP_TXB0SIDL, (message.ID << 5));

  //Sending the DLC and RTR
  CAN_Write(MCP_TXB0DLC, (message.lenght | (message.RTR << 6)));
  //Sending the data
  while (i < message.lenght) {
    cdata = message.data[i];
    CAN_Write(MCP_TXB0D0 + i, cdata);
    i++;
  }

  /* Checking of well transmit message */
  Serial.print("\r\nCAN_TXB0CTRL=");
  Serial.print(CAN_Read(MCP_TXB0CTRL));
//ESTABA COMENTADO
  if((CAN_Read(MCP_TXB0CTRL)|0xBF)==0x80){
    Serial.print("\r\nMensaje enviado correctamente");
    CAN_Write(MCP_TXB0CTRL, 0x03);
  }
  else{
    Serial.print("\r\nMensaje enviado INcorrectamente");
    CAN_Write(MCP_TXB0CTRL, 0x23);
    return 1;
  }
//HASTA AQUÍ

  //Setting up the higher priority and TXREQ=1->Sort Tx
  CAN_Write(MCP_TXB0CTRL, 0x0B);
  delay(500);
  Serial.print("\r\nCAN_TXB0CTRL=");
  Serial.print(CAN_Read(MCP_TXB0CTRL));
  return 0;
}

/**
 * This method allows to read a message of CAN bus through the first buffer
 * RXB0.
 */
messageCAN libCAN::CAN_Read_RXB0() {
  int i = 0;
  messageCAN structMessage;

  Serial.print(CAN_Read(MCP_RXB0CTRL));
  Serial.print("-");
  Serial.print(CAN_Read(MCP_RXB0SIDH));
  Serial.print("-");
  structMessage.ID = MCP_RXB0SIDH >> 3;
  Serial.print(CAN_Read(MCP_RXB0SIDL));
  Serial.print("-");
  structMessage.ID = MCP_RXB0SIDL >> 5;
  Serial.print(CAN_Read(MCP_RXB0DLC));
  structMessage.lenght = MCP_RXB0DLC;
  Serial.print("-");

  do {
    Serial.print(CAN_Read(MCP_RXB0D0 + i));
    Serial.print("-");
    structMessage.data[i] = MCP_RXB0D0 + i;
    i++;
  } while (i <= structMessage.lenght);
  return structMessage;
}

/**
 * This method allows to read a message of CAN bus through the second buffer
 * RXB1.
 */
messageCAN libCAN::CAN_Read_RXB1() {

  int i = 0;
  messageCAN structMessage;

  Serial.print(CAN_Read(MCP_RXB1CTRL));
  Serial.print("-");
  Serial.print(CAN_Read(MCP_RXB1SIDH));
  Serial.print("-");
  structMessage.ID = MCP_RXB1SIDH >> 3;
  Serial.print(CAN_Read(MCP_RXB1SIDL));
  Serial.print("-");
  structMessage.ID = MCP_RXB1SIDL >> 5;
  Serial.print(CAN_Read(MCP_RXB1DLC));
  structMessage.lenght = MCP_RXB1DLC;
  Serial.print("-");

  do {
    Serial.print(CAN_Read(MCP_RXB1D0 + i));
    Serial.print("-");
    structMessage.data[i] = MCP_RXB1D0 + i;
    i++;
  } while (i <= structMessage.lenght);
  return structMessage;
}

libCAN::libCAN() {
  // TODO Auto-generated constructor stub
}

libCAN::~libCAN() {
  // TODO Auto-generated destructor stub
}

libCAN Can = libCAN();
