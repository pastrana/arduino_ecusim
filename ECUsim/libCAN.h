#ifndef LIBCAN_H_
#define LIBCAN_H_

typedef struct {
  unsigned char ID;
  unsigned char RTR;
  unsigned int lenght;
  unsigned char data[];
} messageCAN;

class libCAN {
  public:
    libCAN();
    virtual ~libCAN();
    int reset_MCP();
    int CAN_Write(unsigned char address, unsigned char value);
    unsigned char CAN_Read(unsigned char address);
    int CAN_Init();
    int CAN_Tx_TXB0(messageCAN message);
    messageCAN CAN_Read_RXB0();
    messageCAN CAN_Read_RXB1();
};

extern libCAN Can;

#endif /* LIBCAN_H_ */
