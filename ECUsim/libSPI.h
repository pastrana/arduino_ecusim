#ifndef LIBSPI_H_
#define LIBSPI_H_

class libSPI {
  public:
    libSPI();
    virtual ~libSPI();
    int SPI_Sel();
    int SPI_Unsel();
};

extern libSPI ObjSPI;

#endif /* LIBSPI_H_ */
